package jcode.app.mysql.explain;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

@RequestMapping(value = "/mysql")
@RestController
public class MysqlExplainController {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @GetMapping(value = "query")
    @ResponseBody
    public List query(String sql) throws IOException {
       return  jdbcTemplate.queryForList(sql);
    }

}

