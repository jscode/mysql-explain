<mysql-explain>
    <div>
        <p><span class="sql">explain {opts.sql}</span> ({opts.desc})</p>
        <table class="table">
            <tr>
                <th>id</th>
                <th>select_type</th>
                <th>table</th>
                <th>type</th>
                <th style="width:200px">possible_keys</th>
                <th style="width:100px">key</th>
                <th>key_len</th>
                <th style="width:200px">ref</th>
                <th>rows</th>
                <th style="width:400px">Extra</th>
            </tr>
            <tr each="{data}">
                <td>{id}</td>
                <td>{select_type}</td>
                <td>{table}</td>
                <td>{type}</td>
                <td>{possible_keys}</td>
                <td>{key}</td>
                <td>{key_len}</td>
                <td>{ref}</td>
                <td>{rows}</td>
                <td>{Extra}</td>
            </tr>
        </table>
    </div>
    <script>

        this.data = [];

        var thisComp = this;

        $.getJSON("/mysql/query", {
            sql: 'explain ' + opts.sql
        }, function (data) {
            thisComp.data = data;
            thisComp.update();
        });

    </script>
</mysql-explain>

<mysql-count>
    <span class="count">{count}</span>
    <script>

        this.count = "...";

        var thisComp = this;

        $.getJSON("/mysql/query", {
            sql: 'select count(*) as count from ' + opts.table
        }, function (data) {
            thisComp.count = data[0].count;
            thisComp.update();
        });

    </script>
</mysql-count>

<mysql-version>
    <span>{version}</span>
    <script>

        this.version = "...";

        var thisComp = this;

        $.getJSON("/mysql/query", {
            sql: 'select version() as version'
        }, function (data) {
            console.log(data);
            thisComp.version = data[0].version;
            thisComp.update();
        });

    </script>
</mysql-version>