# mysql-explain
测试各种sql语句，mysql explain运行结果。

# 如何使用？
- 这是一个采用springboot + mysql实现的java web项目
- 默认数据库连接jdbc:mysql://localhost:3306/demo_explain。可以在src/main/resources/db.properties文件中，修改数据库连接配置
- 数据库表初始化脚本，在db.sql文件中
- 项目使用 maven 打包
- 启动应用以后，访问地址 http://localhost/

# 运行效果
![multisources](img/mysql-explain.png)

